
def countMatches(query, subject):
    if(len(query)!=len(subject)):
        print "different length of query or subject"
        return 0
    exactMatches = 0
    for i in range(0,len(query)):
        if(query[i] == subject[i]):
            exactMatches+=1
    return exactMatches

def longest_consecutive(query,subject):
    if(len(query)!=len(subject)):
        print "different length of query or subject"
        return 0
    longestRun = 0;    
    for i in range(0,len(query)):
        tmpRun = 0;
        while(i < len(query) and query[i] == subject[i]):
            tmpRun+=1
            i+=1
        if(tmpRun > longestRun):
            longestRun = tmpRun
    return longestRun